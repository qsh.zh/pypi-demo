# Sphinx

## From source

```shell
sphinx-quickstart # generate conf.py and make file
sphinx-apidoc --ext-viewcode  -o . .. # generates lots of *.rts
make html
```

## quick

* copy the `doc/` to dst project
* remove all rst file
* `m2r2 README.md` and move `README.rst` to `docs/index.rst`
* modify `index.rst`
* modify `conf.py`
