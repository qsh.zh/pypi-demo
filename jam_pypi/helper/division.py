import numpy as np

Array = np.array


def div(x: Array, y: Array) -> Array:
    """div two Array

    :param x: comments for x
    :type x: Array
    :param y: comments for y
    :type y: Array
    :return: summation results
    :rtype: Array
    """
    return x / y
